﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace PasswordStrengthChecker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }
        private void TextBox_TextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void txtPassword_PasswordChanged(object sender, KeyEventArgs e)
        {

        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var Password = txtPassword.Password;
            var collection = new List<String>();
            for (int i = 0; i < collection.Count; i++)
            {
                collection.Add(Password);
            }
        }
        private void GenerateStrengthCount()
        {
            var GetPassword = txtPassword.Password;
            byte[] password = Encoding.UTF8.GetBytes(GetPassword);
            for (int i = 0; i < password.Length; i++)
            {
                String UpperCasePattern = @"[A-Z]";
                String LowerCasePattern = @"[a-z]";
                String Numbers = @"\d{}";
                String DigitCheck = @"[\d]";
                if (Regex.IsMatch(GetPassword.Trim(), UpperCasePattern, RegexOptions.ECMAScript))
                    i = i + 6;
                if (Regex.IsMatch(GetPassword.Trim(), LowerCasePattern, RegexOptions.ECMAScript))
                    i = i + 4;
                if (Regex.IsMatch(GetPassword.Trim(), Numbers, RegexOptions.ECMAScript))
                    i = i + 4;
                if (Regex.IsMatch(GetPassword.Trim(), DigitCheck, RegexOptions.ECMAScript))
                    i = i + 8;
                String parse = i.ToString();
                lblScoreCount.Content = String.Format("Strength: {0}", parse);
                if (i <= 20)
                {
                    txtStrength.IsEnabled = true;
                    txtStrength.Text = String.Format("Very Weak");
                    txtStrength.Background = Brushes.Red;
                }
                if (i > 25) {
                    txtStrength.IsEnabled = true;
                    txtStrength.Text = String.Format("Weak");
                    txtStrength.Background = Brushes.Crimson;
                }
                if(i > 40){
                    txtStrength.IsEnabled = true;
                    txtStrength.Text = String.Format("Strong");
                    txtStrength.Background = Brushes.Gold;
                }
                if(i > 50){
                    txtStrength.IsEnabled = true;
                    txtStrength.Text = String.Format("Very Strong");
                    txtStrength.Background = Brushes.Green;
                }
                var items = new List<PasswordMeter>();
                items.Add(new PasswordMeter(GetPassword, i, GetPasswordInfo()));
                foreach(var temp in items){
                    dtaDisplay.ItemsSource = items;
                }
                //also will have to set the textbox colours and text to display. 
                //Score key Uppercase letter = +4;
                // 3 or 4 - Lowercase letters = >3 - weaker score
                // 3 or 4 - Lowercase letters = <3 - slightly stronger
                // 3 or 4 - Numbers - <3 lesser score and >3 stronger score
                // <15- very weak
                // > 30 - weak
                // > 45 - Strong
                // > 75 - Very Strong . may need to define ranges when enumerating once enumerating then spit to textbox along with changing colour
                //possible combinations as well such as Numbers only -length
                //Consecutive upper-case letters -4
            }
        }
        private void BtnEnter_Click(object sender, RoutedEventArgs e)
        {
            if (txtPassword.Password == String.Empty)
                MessageBox.Show("Please Enter a Password with the following spec as listed. Minimum Requirements: 8 characters in length, Contains 3/4 of the following items:/n -Uppercase Letters/n -Lowercase Letters/n -Numbers/n Symbols", "Password must be Entered", MessageBoxButton.OK);
            GenerateStrengthCount();
        }
        public static String GetPasswordInfo()
        {
            String text = String.Format("Criteria for a strong password is 1 Uppercase letter = n + 4.\n");
            text += String.Format("Must also contain 3 or 4 Lowercase letters = n +3.\n");
            text += String.Format("Must also contain Numbers n + 2.");
            text += String.Format("Can contain symbols n + 6");
            //take into account symbols
            return text;
        }
        private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}

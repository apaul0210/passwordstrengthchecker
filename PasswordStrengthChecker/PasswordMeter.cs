﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordStrengthChecker
{
    class PasswordMeter
    {
        private String Password;
        private int ScoreCount;
        private String PasswordRule;

        public PasswordMeter(String password, int Scorecount, String passwordRule)
        {
            this.Password = password;
            this.ScoreCount = Scorecount;
            this.PasswordRule = passwordRule;
        }
    }
}
